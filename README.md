# Track My Trek
## Demo

![Track My Trek](pics/trackmytrek.gif "Track My Trek")

## Description
Trackmytrek is a cross platform mobile app I wrote to help me learn Flutter and to help me keep track of the walking/running routes I've already taken.

Major features completed are:
* Works on iphone and android
* Keeps track of route as you walk (using GPS)
* Stores tracked routes (permanently and encrypted, using sqlite)
* Google map displaying location
* Keeps track of time spent walking (cumulative and current) as well as distance walked (using activity recognition)
* Allows setting height and gender to calibrate pedometer (using SharedPreferences)
* Allows picking different map types (satellite vs normal vs hybrid)
* Guides through setting permissions for walking/GPS (using permissions)
* Test ads for potential monetization

Other features I may implement in the future are:
* Learn flutter tests and add them 
* Turn actual ads on
* Release on the app store

## Running Locally
1. Open the project in android studio
2. Run it against your device or emulator

## Attribution
I did not write the path smoothing code for tracked routes, I sourced it from 
https://github.com/darwin-morocho/douglas-peucker-dart/blob/3a2f089b0172b331463c99766603a3ebb2b1f673/lib/src/douglas_peucker.dart

Icons used are all free/do not require contribution, but were sourced from
* https://uxwing.com/running-walk-icon/
* https://uxwing.com/hiking-icon/

I used a different computer(mac) to do the iphone testing, hence the different git commit names
