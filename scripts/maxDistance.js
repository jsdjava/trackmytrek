const distances = require('../data/allDistances.json');

let maxDistance;

for(const distance of distances){
  if(!maxDistance || distance.thing.length > maxDistance.thing.length){
    maxDistance = distance;
  }
}

console.log(JSON.stringify(maxDistance,null,2));