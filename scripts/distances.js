const fs = require('fs');

const distancesTxt = fs.readFileSync("../data/distances.txt").toString("utf8");

const distances = distancesTxt.split("\n").map(r=>r.split(","));
const factors = [2,3,4,5,10,20,50,100,1000,10000,100000,1000000,10000000];

let allDistances = [];

function addDistance(y){
  const matchedDistance = allDistances.find(x=>x.distance == y.distance);
  if(!matchedDistance || y.ranking < matchedDistance.ranking){
    allDistances.push(y);
  }
}

for(let [distance,thing] of distances){
  distance = parseFloat(distance);
  thing = thing.trim();
  addDistance({
    distance,
    thing,
    ranking:1,
  });
  for(let factor of factors){
    const prettyFactor = factor.toLocaleString();
    addDistance({
      distance:distance*factor,
      thing:`${prettyFactor}x ${thing}`,
      ranking: factor,
    });
    addDistance({
      distance:distance/factor,
      thing:`1/${prettyFactor}${factor>=4 ? "th" : ""} ${thing}`,
      ranking: factor,
    });
  }
}

allDistances = allDistances
  .sort(({distance:d1},{distance:d2})=>d1-d2)
  .filter(({distance})=>distance > 0.01);

fs.writeFileSync("../data/allDistances.json",JSON.stringify(allDistances,null,2));
