import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:geolocator/geolocator.dart';
import 'package:pedometer/pedometer.dart';
import 'package:provider/provider.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';

import 'package:track_my_trek/providers/database.dart';
import 'package:track_my_trek/providers/images.dart';
import 'package:track_my_trek/providers/json.dart';
import 'package:track_my_trek/providers/settings.dart';
import 'package:track_my_trek/providers/steps.dart';
import 'package:track_my_trek/providers/location.dart';

import 'package:track_my_trek/model/LockModel.dart';
import 'package:track_my_trek/model/PathModel.dart';
import 'package:track_my_trek/model/PathsModel.dart';
import 'package:track_my_trek/model/SettingsModel.dart';

import 'package:track_my_trek/widgets/DistanceLabel.dart';
import 'package:track_my_trek/widgets/PathsLabel.dart';
import 'package:track_my_trek/widgets/ScreenLock.dart';
import 'package:track_my_trek/widgets/TimerLabel.dart';
import 'package:track_my_trek/widgets/TrackButton.dart';
import 'package:track_my_trek/widgets/SettingsButton.dart';

void main() {
  if (defaultTargetPlatform == TargetPlatform.android) {
    final MethodChannelGoogleMapsFlutter platform =
    GoogleMapsFlutterPlatform.instance as MethodChannelGoogleMapsFlutter;
    platform.useAndroidViewSurface = true;
  }
  WidgetsFlutterBinding.ensureInitialized();
  MobileAds.instance.initialize();
  var locationStream = Geolocator.getPositionStream(
      desiredAccuracy: LocationAccuracy.best, distanceFilter: 5);
  var stepsStream = Pedometer.stepCountStream;
  var timeStream =
  Stream.periodic(new Duration(milliseconds: 500), (_) => "ping")
      .asBroadcastStream();
  runApp(MultiProvider(
      providers: [
        Provider<DBProvider>(create: (_) => DBProvider.db),
        Provider<SettingsProvider>(
            create: (_) => SettingsProvider.settingsProvider),
        FutureProvider<BitmapDescriptor>(
          create: (_) =>
              ImagesProvider.imagesProvider
                  .getImage('graphics/running-walk.png'),
          initialData: BitmapDescriptor.defaultMarker,
        ),
        FutureProvider<List>(
          create: (_) =>
              JsonProvider.jsonProvider.loadJson('data/allDistances.json'),
          initialData: [],
        ), //await Permission.activityRecognition.status.isGranted;
        ChangeNotifierProvider<LocationProvider>(
          create: (_) => new LocationProvider(locationStream: locationStream),
        ),
        ChangeNotifierProvider<StepsProvider>(
            create: (_) => new StepsProvider(stepsStream: stepsStream))
      ],
      child: MultiProvider(providers: [
        ChangeNotifierProxyProvider2<LocationProvider, DBProvider, LockModel>(
            create: (context) =>
            new LockModel(
                Provider.of<LocationProvider>(context, listen: false),
                Provider.of<DBProvider>(context, listen: false),
                null),
            update: (_, locationProvider, dbProvider, oldLockModel) =>
            new LockModel(locationProvider, dbProvider, oldLockModel)),
        ChangeNotifierProxyProvider2<LocationProvider,
            StepsProvider,
            PathModel>(
            create: (context) =>
                PathModel(
                    timeStream,
                    Provider.of<DBProvider>(context, listen: false),
                    Provider.of<LocationProvider>(context, listen: false),
                    Provider.of<StepsProvider>(context, listen: false),
                    null),
            update: (context, locationProvider, stepsProvider, oldPathModel) =>
                PathModel(
                    timeStream,
                    Provider.of<DBProvider>(context, listen: false),
                    Provider.of<LocationProvider>(context, listen: false),
                    Provider.of<StepsProvider>(context, listen: false),
                    oldPathModel)),
        ChangeNotifierProvider(
            create: (context) =>
                PathsModel(Provider.of<DBProvider>(context, listen: false))),
        ChangeNotifierProvider(
            create: (context) =>
                SettingsModel(
                    Provider.of<SettingsProvider>(context, listen: false)))
      ], child: MyApp())));
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  GoogleMapController? mapController;
  LatLng _center = const LatLng(45.521563, -122.677433);

  final BannerAd myBanner = BannerAd(
    adUnitId: 'ca-app-pub-3940256099942544/6300978111',
    size: AdSize.banner,
    request: AdRequest(),
    listener: AdListener(),
  );

  @override
  void initState() {
    myBanner.load();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var distances = Provider.of<List>(context);
    var runningWalk = Provider.of<BitmapDescriptor>(context);
    var points = context.select<PathModel, List<LatLng>>((p) => p.points);
    Set<Marker> markers = points.isNotEmpty
        ? {
      new Marker(
          markerId: new MarkerId("marker"),
          zIndex: 2,
          position:
          new LatLng(points.last.latitude, points.last.longitude),
          icon: runningWalk)
    }
        : {};
    return MaterialApp(
        home: Scaffold(
          appBar: AppBar(
            title: Text('TrackMyTrek'),
            backgroundColor: Colors.green[700],
          ),
          body: new Container(
              child: Column(children: <Widget>[
                Container(
                    child: AdWidget(
                      ad: myBanner,
                      key: new Key("Ad"),
                    ),
                    height: 60),
                Expanded(
                    child: new Stack(children: <Widget>[
                      new Positioned(
                          child: Listener(
                            key: new Key("OnPointerMoveListener"),
                            onPointerMove: (move) {
                              Provider
                                  .of<LockModel>(context, listen: false)
                                  .locked = false;
                            },
                            child: Consumer<SettingsModel>(
                                builder: (_, settingsModel, __) =>
                                    Consumer<PathsModel>(
                                        builder: (_, pathsModel, __) =>
                                        new GoogleMap(
                                          key: Key("GoogleMap"),
                                          mapType: settingsModel.trackMapType
                                              .mapType,
                                          markers: markers,
                                          polylines: {
                                            ...List.generate(
                                                pathsModel.previousPaths.length,
                                                    (i) =>
                                                    pathsModel.previousPaths[i]
                                                        .getPolyLine()),
                                            new Polyline(
                                              polylineId: new PolylineId(
                                                  "CurrentPath"),
                                              points: List.generate(
                                                  points.length,
                                                      (i) =>
                                                  new LatLng(
                                                      points[i].latitude,
                                                      points[i].longitude)),
                                              width: 4,
                                              color: Colors.red,
                                              zIndex: 1,
                                            ),
                                          },
                                          /*onTap: (LatLng latLng) {
                          var pathModel =
                              Provider.of<PathModel>(context, listen: false);
                          pathModel.onPositionChange(new Position(
                            latitude: latLng.latitude,
                            longitude: latLng.longitude,
                            accuracy: 0,
                            altitude: 0,
                            heading: 0,
                            speed: 0,
                            speedAccuracy: 0,
                            timestamp: DateTime.now(),
                          ));
                        }*/
                                          onMapCreated: (mapController) =>
                                              Provider.of<LockModel>(
                                                  context, listen: false)
                                                  .registerMapController(
                                                  mapController),
                                          initialCameraPosition: CameraPosition(
                                            target: _center,
                                            zoom: 16,
                                          ),
                                        ))),
                          )),
                      // A little weird, SettingsModel is listening to both providers, and yet the button
                      // is listening to all 3. Maybe an indicator that state needs to move from the providers
                      // to settings model...
                      // Also, could just be written with context.select...
                      Consumer<StepsProvider>(
                          builder: (_, stepsProvider, __) =>
                              Consumer<LocationProvider>(
                                  builder: (_, locationProvider, __) =>
                                      Consumer<SettingsModel>(
                                        builder: (_, settingsModel, __) =>
                                        new Positioned(
                                          child: new SettingsButton(
                                            locationStatus: locationProvider
                                                .locationStatus,
                                            stepsEnabled: stepsProvider
                                                .stepsEnabled,
                                            stepsPermanentlyDisabled:
                                            stepsProvider
                                                .stepsPermanentlyDisabled,
                                            settings: settingsModel,
                                          ),
                                          right: 10,
                                        ),
                                      ))),
                      Padding(
                          padding: EdgeInsets.only(left: 10, top: 10),
                          child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                new DistanceLabel(
                                  steps: context.select<SettingsModel,
                                      Distance>(
                                          (settingsModel) =>
                                      settingsModel.distance) ==
                                      Distance.CUMULATIVE
                                      ? (context.select<PathModel, bool>((
                                      PathModel p) => p.tracking)
                                      ? context.select<PathsModel, int>(
                                          (PathsModel p) => p.previousSteps) +
                                      context.select<PathModel, int>(
                                              (PathModel p) => p.steps)
                                      : context.select<PathsModel, int>(
                                          (PathsModel p) => p.previousSteps))
                                      : context
                                      .select<PathModel, int>((PathModel p) =>
                                  p.steps),
                                  distances: distances,
                                  height: context.select<SettingsModel, int>(
                                          (SettingsModel settings) =>
                                      settings.height),
                                  sex: context.select<SettingsModel, Sex>(
                                          (SettingsModel settings) =>
                                      settings.sex),
                                ),
                                SizedBox(height: 10),
                                new TimeLabel(
                                    time: context.select<SettingsModel,
                                        Distance>(
                                            (settingsModel) =>
                                        settingsModel.distance) ==
                                        Distance.CUMULATIVE
                                        ? (context.select<PathModel, bool>((
                                        PathModel p) => p.tracking) ? context
                                        .select<PathsModel, int>(
                                            (pathsModel) => pathsModel.time) +
                                        context.select<PathModel, int>(
                                                (pathModel) =>
                                            pathModel.elapsedTime) : context
                                        .select<
                                        PathsModel,
                                        int>(
                                            (pathsModel) => pathsModel.time))
                                        : context.select<PathModel, int>(
                                            (pathModel) =>
                                        pathModel.elapsedTime)),
                                SizedBox(height: 10),
                                new PathsLabel(
                                  totalPaths: context.select<PathsModel, int>(
                                          (PathsModel p) => p.totalPaths),
                                  curPaths: context.select<PathsModel, int>(
                                          (PathsModel p) =>
                                      p.previousPaths.length),
                                ),
                              ])),
                      new Positioned(
                          bottom: 40,
                          child: new Row(children: <Widget>[
                            Consumer<PathModel>(
                                builder: (_, pathModel, __) =>
                                    TrackButton(
                                      lockModel:
                                      Provider.of<LockModel>(
                                          context, listen: false),
                                      pathModel: pathModel,
                                      pathsModel:
                                      Provider.of<PathsModel>(
                                          context, listen: false),
                                    )),
                            new Consumer<LockModel>(
                                builder: (_, lockModel, __) =>
                                    ScreenLock(lockModel: lockModel))
                          ])),
                    ]))
              ])),
        ));
  }
}
