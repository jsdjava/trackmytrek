import 'package:flutter/material.dart';

import '../model/LockModel.dart';
import '../model/PathModel.dart';
import '../model/PathsModel.dart';

class TrackButton extends StatelessWidget {
  final PathModel pathModel;
  final PathsModel pathsModel;
  final LockModel lockModel;

  TrackButton(
      {required this.pathModel,
      required this.pathsModel,
      required this.lockModel});

  @override
  Widget build(BuildContext context) {
    return IconButton(
      iconSize: 40,
      icon:
          Icon(pathModel.tracking ? Icons.directions_walk : Icons.not_started),
      tooltip: pathModel.tracking ? "Start tracking" : "Stop tracking",
      onPressed: () {
        if (pathModel.tracking) {
          if (pathModel.points.length >=2) {
            pathsModel.addPath(pathModel.getPath());
          }
          pathModel.stopTracking();
        } else {
          lockModel.locked = true;
          pathModel.startTracking();
        }
      },
    );
  }
}
