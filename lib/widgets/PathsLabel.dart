import 'package:flutter/widgets.dart';

class PathsLabel extends StatelessWidget {
  final int totalPaths;
  final int curPaths;

  PathsLabel({required this.totalPaths, required this.curPaths});

  @override
  Widget build(BuildContext context) {
    var loadedPaths = "$curPaths/$totalPaths paths";
    return Text(loadedPaths,
        style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 15,
            backgroundColor: Color.fromARGB(200, 255, 255, 255)));
  }
}
