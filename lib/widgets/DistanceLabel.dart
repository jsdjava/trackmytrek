import 'package:flutter/material.dart';

import '../providers/settings.dart';

class DistanceLabel extends StatelessWidget {
  final int steps;
  final List distances;
  final int height;
  final Sex sex;

  DistanceLabel({
    required this.steps,
    required this.distances,
    required this.height,
    required this.sex,
  });

  @override
  Widget build(BuildContext context) {
    var sexFactor = sex == Sex.FEMALE ? 0.413 : 0.415;
    var milesNum = (steps * sexFactor * this.height) / (5280 * 12);
    var index = distances.indexWhere((x) => x['distance'] > milesNum);
    var distanceLabel = "";
    if (index > 0) {
      distanceLabel = "That's >" +
          distances[index - 1]['thing'] +
          "!"; //"> 10,000,000x the Hollywood Walk of Fame";
    }
    String miles = milesNum.toStringAsFixed(2);
    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Text(
            "$miles miles",
            //textAlign: TextAlign.left,
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 20,
              backgroundColor: Color.fromARGB(200, 255, 255, 255),
            ),
          ),
          new SizedBox(height: 10),
          Text(distanceLabel,
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 15,
                  backgroundColor: Color.fromARGB(200, 255, 255, 255)))
        ]);
  }
}
