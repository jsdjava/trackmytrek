import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:track_my_trek/providers/location.dart';

import '../providers/settings.dart';
import '../providers/steps.dart';
import '../model/SettingsModel.dart';

class SettingsButton extends StatelessWidget {
  final SettingsModel settings;
  final bool stepsEnabled;
  final bool stepsPermanentlyDisabled;
  final LocationStatus locationStatus;
  final String permanentlyDeniedError = "Permanently denied";
  static bool showingSettingsPopup = false;

  SettingsButton(
      {required this.settings,
      required this.locationStatus,
      required this.stepsEnabled,
      required this.stepsPermanentlyDisabled});

  showSettingsDialog(BuildContext context, {bool sticky = false}) {
    showDialog(
        barrierDismissible: !sticky,
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            scrollable: true,
            title: new Text("Settings"),
            content: Form(
              key: new Key("SettingsPopup"),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Padding(
                      padding: EdgeInsets.all(8.0),
                      child: Row(children: <Widget>[
                        Text("Height (in):"),
                        Expanded(
                          child: TextFormField(
                              textAlign: TextAlign.right,
                              initialValue: settings.height.toString(),
                              onChanged: (x) {
                                settings.height =
                                    int.tryParse(x) ?? settings.height;
                              },
                              keyboardType: TextInputType.number,
                              inputFormatters: <TextInputFormatter>[
                                FilteringTextInputFormatter.allow(
                                    RegExp(r'[0-9]')),
                              ],
                              decoration: InputDecoration.collapsed(
                                  hintText: 'Height (in.)')),
                        )
                      ])),
                  Padding(
                      padding: EdgeInsets.all(8.0),
                      child: Row(children: <Widget>[
                        Expanded(child: Text("Gender:")),
                        ToggleButtons(
                          children: <Widget>[
                            Icon(Icons.male),
                            Icon(Icons.female),
                          ],
                          onPressed: (int index) {
                            settings.sex = index == 0 ? Sex.MALE : Sex.FEMALE;
                          },
                          isSelected: [
                            settings.sex == Sex.MALE,
                            settings.sex == Sex.FEMALE
                          ],
                        ),
                      ])),
                  Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Row(children: <Widget>[
                      Expanded(child: Text("Distance:")),
                      ToggleButtons(
                        children: <Widget>[
                          Text("Current"),
                          Text("Cumulative"),
                        ],
                        onPressed: (int index) {
                          settings.distance = index == 0
                              ? Distance.CURRENT
                              : Distance.CUMULATIVE;
                        },
                        isSelected: [
                          settings.distance == Distance.CURRENT,
                          settings.distance == Distance.CUMULATIVE
                        ],
                      )
                    ]),
                  ),
                  Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Row(children: <Widget>[
                      Expanded(child: Text("Map Style:")),
                      new DropdownButton(
                          value: settings.trackMapType,
                          onChanged: (TrackMapType? trackMapType) {
                            if (trackMapType != null) {
                              settings.trackMapType = trackMapType;
                            }
                          },
                          items: TrackMapType.values
                              .map<DropdownMenuItem<TrackMapType>>(
                                  (TrackMapType trackMapType) {
                            return DropdownMenuItem<TrackMapType>(
                                value: trackMapType,
                                child: Text(trackMapType.toPrettyString()));
                          }).toList())
                    ]),
                  ),
                  Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Row(children: <Widget>[
                      Expanded(
                          child: locationStatus.toPrettyString() != null
                              ? Text(locationStatus.toPrettyString()!)
                              : Icon(locationStatus == LocationStatus.WORKING
                                  ? Icons.check_circle
                                  : Icons.cancel_rounded)),
                      locationStatus == LocationStatus.DISABLED
                          ? TextButton(
                              child: Text("Enable GPS"),
                              onPressed: () => Provider.of<LocationProvider>(
                                      context,
                                      listen: false)
                                  .enableLocation())
                          : Text("GPS Permissions")
                    ]),
                  ),
                  Padding(
                      padding: EdgeInsets.all(8.0),
                      child: Row(
                        children: <Widget>[
                          Expanded(
                              child: stepsPermanentlyDisabled
                                  ? Text(permanentlyDeniedError)
                                  : Icon(stepsEnabled
                                      ? Icons.check_circle
                                      : Icons.cancel_rounded)),
                          !(stepsPermanentlyDisabled || stepsEnabled)
                              ? TextButton(
                                  child: Text("Enable Steps"),
                                  onPressed: () => Provider.of<StepsProvider>(
                                          context,
                                          listen: false)
                                      .enableSteps())
                              : Text("Steps Permissions")
                        ],
                      )),
                  if (!(stepsEnabled &&
                      locationStatus == LocationStatus.WORKING))
                    Row(children: <Widget>[
                      Expanded(
                          child: Text(
                              locationStatus ==
                                          LocationStatus.PERMANENTLY_DISABLED ||
                                      locationStatus ==
                                          LocationStatus.TURNED_OFF ||
                                      stepsPermanentlyDisabled
                                  ? "*GPS or Pedometer have been permanently disabled or manually turned off. To fix this, close TrackMyTrek, manually open your phone's Settings Manager, enable GPS and Pedometer for TrackMyTrek, and reopen TrackMyTrek"
                                  : "*GPS and Pedometer must be enabled for TrackMyTrek to work.",
                              style: TextStyle(
                                  color: Colors.deepOrange, fontSize: 12)))
                    ]),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ElevatedButton(
                      child: Text("Done"),
                      onPressed: locationStatus == LocationStatus.WORKING &&
                              stepsEnabled
                          ? () {
                              SettingsButton.showingSettingsPopup = false;
                              Navigator.of(context).pop(true);
                            }
                          : null,
                    ),
                  )
                ],
              ),
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    if (!(this.locationStatus == LocationStatus.WORKING && this.stepsEnabled)) {
      if (this.locationStatus != LocationStatus.INITIALIZING && !SettingsButton.showingSettingsPopup) {
        SettingsButton.showingSettingsPopup = true;
        Future.delayed(
            Duration.zero, () => showSettingsDialog(context, sticky: true));
      }
    }
    return IconButton(
        iconSize: 40,
        icon: Icon(Icons.settings),
        tooltip: "Settings",
        onPressed: () => this.showSettingsDialog(context));
  }
}
