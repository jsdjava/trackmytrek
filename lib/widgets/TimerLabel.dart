import 'package:flutter/widgets.dart';

class TimeLabel extends StatelessWidget {
  final int time;

  TimeLabel({required this.time});

  static const secsInMins = 60;
  static const secsInHours = 60 * secsInMins;
  static const secsInDays = 24 * secsInHours;

  @override
  Widget build(BuildContext context) {
    var days = ((time / secsInDays).floor()).toString().padLeft(2, "0");
    var hours =
        ((time % secsInDays) / secsInHours).floor().toString().padLeft(2, "0");
    var minutes =
        ((time % secsInHours) / secsInMins).floor().toString().padLeft(2, "0");
    var seconds = (time % secsInMins).floor().toString().padLeft(2, "0");
    var formattedTime = "$hours:$minutes:$seconds";
    if (days != "00") {
      formattedTime = days + formattedTime;
    }
    return Text(formattedTime,
        style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 15,
            backgroundColor: Color.fromARGB(200, 255, 255, 255)));
  }
}
