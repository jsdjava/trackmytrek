import 'package:flutter/material.dart';

import '../model/LockModel.dart';

class ScreenLock extends StatelessWidget {
  final LockModel lockModel;

  ScreenLock({required this.lockModel});

  @override
  Widget build(BuildContext context) {
    return IconButton(
      iconSize: 40,
      icon: Icon(lockModel.locked ? Icons.lock : Icons.lock_open),
      tooltip: lockModel.locked ? "Unlock screen" : "Lock screen",
      onPressed: () {
        lockModel.locked = !lockModel.locked;
      },
    );
  }
}
