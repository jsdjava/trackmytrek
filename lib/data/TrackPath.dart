import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class TrackPath {
  final int id;
  final int steps;
  final int time;
  late Polyline polyline;
  final List<LatLng> points;

  TrackPath({
    required this.id,
    required this.steps,
    required this.points,
    required this.time,
    Color color = Colors.lightBlue,
    int zIndex = 0
  }){
    polyline = new Polyline(
        polylineId: new PolylineId("$id"),
        width: 2,
        color: color,
        points: points,
        zIndex: zIndex
    );
  }

  getPolyLine() {
    return polyline;
  }
}
