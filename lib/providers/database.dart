//i.e. https://github.com/ericgrandt/flutter-streams/blob/master/lib/data/database.dart

import 'dart:math';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:random_string_generator/random_string_generator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:path/path.dart';

import 'package:sqflite_sqlcipher/sqflite.dart';

import '../data/TrackPath.dart';

class DBProvider {
  DBProvider._();

  static final DBProvider db = DBProvider._();
  Database? _database;
  final _storage = FlutterSecureStorage();

  Future<Database> get database async {
    if (_database != null) {
      return _database!;
    }

    _database = await initDB();
    //floodDB();
    return _database!;
  }

  // 41.8781° N, 87.6298° W
  floodDB() async {
    for (var i = 0; i < 1000; i++) {
      var pathId = await createPath();
      var batch = (await database).batch();
      for (var j = 0; j < 50; j++) {
        var latitude = 41.8781 + Random().nextDouble() * 10 - 5;
        var longitude = -87.6298 + Random().nextDouble() * 10 - 5;
        batch.execute(
            "INSERT INTO track_points (track_path_id,latitude,longitude) values (${pathId}, ${latitude}, ${longitude})");
        //print("At Point $j");
      }
      await batch.commit();
      await updatePathSteps(pathId, Random().nextInt(10000));
      await updatePathTime(pathId, Random().nextInt(1000));
      print("At Path $i");
    }
  }

  Future<int> getDbTotalPaths() async {
    var db = await database;
    var result = await db.rawQuery("select count(*) from track_paths");
    return Sqflite.firstIntValue(result)!;
  }

  Future<int> getDbSteps() async {
    var db = await database;
    var result = await db.rawQuery("select sum(steps) from track_paths");
    return Sqflite.firstIntValue(result) ?? 0;
  }

  Future<int> getDbTime() async {
    var db = await database;
    var result = await db.rawQuery("select sum(time) from track_paths");
    return Sqflite.firstIntValue(result) ?? 0;
  }

  Future<LatLng?> getFirstDbPosition() async {
    var db = await database;
    List<Map<String, dynamic>> trackPointResults =
        await db.query('track_points', orderBy: "id", limit: 1);
    if (trackPointResults.isNotEmpty) {
      return new LatLng(trackPointResults.first['latitude'],
          trackPointResults.first['longitude']);
    }
    return null;
  }

  Stream<List<TrackPath>> getDbPathsStream() async* {
    var db = await database;
    var chunkSize = 10;
    var result = await db.rawQuery("select count(*) from track_paths");
    var totalPaths = Sqflite.firstIntValue(result) ?? 0;
    for (var i = 0; i < (totalPaths / chunkSize).ceil(); i++) {
      List<Map<String, dynamic>> trackPathResults = await db.query(
          'track_paths',
          orderBy: "id",
          limit: chunkSize,
          offset: i * chunkSize);
      var idsStr = trackPathResults.map((x) => x['id']).join(",");
      List<Map<String, dynamic>> trackPointResults = await db.rawQuery(
          'select * from track_points where track_path_id in ($idsStr)');
      List<TrackPath> trackPaths = [];
      for (var j = 0; j < trackPathResults.length; j++) {
        var points = trackPointResults
            .where((x) => x['track_path_id'] == trackPathResults[j]['id'])
            .map((x) => new LatLng(x['latitude'], x['longitude']))
            .toList();
        trackPaths.add(new TrackPath(
            id: trackPathResults[j]['id'],
            steps: trackPathResults[j]['steps'],
            time: trackPathResults[j]['time'],
            points: points));
      }
      yield trackPaths;
      await new Future.delayed(const Duration(milliseconds: 25));
    }
  }

  List<TrackPath> getRandomPaths() {
    //10,000paths
    //3000 points each
    List<TrackPath> paths = [];
    for (var i = 0; i < 5000; i++) {
      List<LatLng> points = [];
      for (var j = 0; j < 50; j++) {
        double latitude =
            41.8781 + (j > 0 ? Random().nextDouble() * 10 - 5 : 0);
        double longitude =
            -87.6298 + (j > 0 ? Random().nextDouble() * 10 - 5 : 0);
        points.add(new LatLng(latitude, longitude));
      }
      paths.add(new TrackPath(
          id: i,
          points: points,
          steps: Random().nextInt(10000),
          time: Random().nextInt(1000)));
      print("At Path $i");
    }
    return paths;
  }

  initDB() async {
    const passwordKey = "track_my_trek_password";
    String password;

    // TODO Can the user accidentally clear this key?
    if (await _storage.containsKey(key: passwordKey)) {
      password = (await _storage.read(key: passwordKey))!;
    } else {
      password = RandomStringGenerator(fixedLength: 32).generate();
      await _storage.write(key: passwordKey, value: password);
    }
    return await openDatabase(
        join(await getDatabasesPath(), 'track_my_trekv9.db'),
        password: password, onCreate: (db, version) async {
      await db.execute(
          "CREATE TABLE track_paths(id INTEGER PRIMARY KEY, steps INTEGER, time INTEGER)");
      await db.execute(
          "CREATE TABLE track_points(id INTEGER PRIMARY KEY, latitude DOUBLE, longitude DOUBLE, track_path_id INTEGER, FOREIGN KEY (track_path_id) REFERENCES track_paths(id))");
    }, version: 1);
  }

  Stream<List<TrackPath>> getPathsStream() async* {
    var chunkSize = 100;
    List<TrackPath> paths = [];
    for (var i = 0; i < 1000; i++) {
      List<LatLng> points = [];
      for (var j = 0; j < 50; j++) {
        double latitude =
            41.8781 + (j > 0 ? Random().nextDouble() * 10 - 5 : 0);
        double longitude =
            -87.6298 + (j > 0 ? Random().nextDouble() * 10 - 5 : 0);
        points.add(new LatLng(latitude, longitude));
      }
      if (i % chunkSize == 0) {
        yield paths;
        await new Future.delayed(const Duration(milliseconds: 50));
        paths.clear();
      }
      paths.add(new TrackPath(
          id: i,
          points: points,
          steps: Random().nextInt(10000),
          time: Random().nextInt(1000)));
      print("At Path $i");
    }
  }

  Future<List<TrackPath>> getPaths() async {
    //return [];
    return getRandomPaths();
    /*final db = await database;
    /*List<Map<String, dynamic>> pointMaps
    var page = [];
    do{
      page = await db.query('track_paths',orderBy:"id",whereArgs:)
      pointMaps.addAll(page);
    } while(page.isNotEmpty);*/
    List<Map<String, dynamic>> pointMaps = await db.query('track_points');
    List<Map<String, dynamic>> pathMaps = await db.query('track_paths');

    List<TrackPoint> points = List.generate(pointMaps.length, (i) {
      return TrackPoint(
          id: pointMaps[i]['id'],
          latitude: pointMaps[i]['latitude'],
          longitude: pointMaps[i]['longitude'],
          track_path_id: pointMaps[i]['track_path_id']);
    });
    Map<int, List<TrackPoint>> pointMapsByPath =
        groupBy(points, (x) => x.track_path_id);

    return List.generate(pathMaps.length, (i) {
      return TrackPath(
          id: pathMaps[i]['id'],
          steps: pathMaps[i]['steps'],
          points: pointMapsByPath[pathMaps[i]['id']] ?? []);
    });*/
  }

  addPointToPath(LatLng point, int trackPathId) async {
    final db = await database;
    return db.execute(
        "INSERT INTO track_points (track_path_id,latitude,longitude) values (${trackPathId}, ${point.latitude}, ${point.longitude})");
  }

  Future<int> createPath() async {
    final db = await database;
    var id = await db.insert("track_paths", {'steps': 0, 'time':0});
    return id;
  }

  updatePathTime(int id, int time) async {
    final db = await database;
    await db.update("track_paths", {'time': time},
        where: 'id = ?', whereArgs: [id]);
  }

  updatePathSteps(int id, int steps) async {
    final db = await database;
    await db.update("track_paths", {'steps': steps},
        where: 'id = ?', whereArgs: [id]);
  }
}
