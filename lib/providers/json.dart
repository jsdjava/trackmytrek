import 'dart:convert';

import 'package:flutter/services.dart';

class JsonProvider {
  JsonProvider._();

  static final JsonProvider jsonProvider = new JsonProvider._();

  Map<String, List> _jsonFiles = new Map();

  Future<List> loadJson(String path) async {
    if (_jsonFiles.containsKey(path)) {
      return _jsonFiles[path]!;
    }
    var jsonText = await rootBundle.loadString(path);
    _jsonFiles[path] = json.decode(jsonText);
    return _jsonFiles[path]!;
  }
}
