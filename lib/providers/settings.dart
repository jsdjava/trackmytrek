import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';

enum Sex { MALE, FEMALE }
enum Distance {
  CUMULATIVE,
  CURRENT,
}
enum TrackMapType {
  HYBRID,
  NORMAL,
  SATELLITE,
  TERRAIN,
}

extension TrackMapTypeMapper on TrackMapType {
  MapType get mapType {
    switch (this) {
      case TrackMapType.HYBRID:
        return MapType.hybrid;
      case TrackMapType.SATELLITE:
        return MapType.satellite;
      case TrackMapType.TERRAIN:
        return MapType.terrain;
    }
    return MapType.normal;
  }
  String toPrettyString(){
    switch (this) {
      case TrackMapType.HYBRID:
        return "Hybrid";
      case TrackMapType.SATELLITE:
        return "Satellite";
      case TrackMapType.TERRAIN:
        return "Terrain";
    }
    return "Normal";
  }
}

class SettingsProvider {
  SettingsProvider._();

  static final SettingsProvider settingsProvider = SettingsProvider._();

  SharedPreferences? _settings;

  Future<SharedPreferences> get settings async {
    if (_settings != null) {
      return _settings!;
    }
    _settings = await initSettings();
    return _settings!;
  }

  initSettings() async {
    return await SharedPreferences.getInstance();
  }

  get(key) async {
    final curSettings = await settings;
    return curSettings.get(key);
  }

  saveInt(key, value) async {
    final curSettings = await settings;
    curSettings.setInt(key, value);
  }

  saveString(key, value) async {
    final curSettings = await settings;
    curSettings.setString(key, value);
  }
}
