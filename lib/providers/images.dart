import 'dart:ui' as ui;

import 'package:flutter/services.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class ImagesProvider {
  ImagesProvider._();

  static final ImagesProvider imagesProvider = new ImagesProvider._();

  Map<String, BitmapDescriptor> _images = new Map();

  Future<BitmapDescriptor> getImage(String path) async {
    if (_images.containsKey(path)) {
      return _images[path]!;
    }
    _images[path] = await _loadImage(path);
    return _images[path]!;
  }

  Future<BitmapDescriptor> _loadImage(String path) async {
    var width = 40;
    ByteData data = await rootBundle.load(path);
    ui.Codec codec = await ui.instantiateImageCodec(data.buffer.asUint8List(),
        targetWidth: width);
    ui.FrameInfo fi = await codec.getNextFrame();
    var bytes = (await fi.image.toByteData(format: ui.ImageByteFormat.png))!
        .buffer
        .asUint8List();
    return BitmapDescriptor.fromBytes(bytes);
  }
}
