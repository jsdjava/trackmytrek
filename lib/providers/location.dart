import 'package:flutter/cupertino.dart';
import 'package:geolocator/geolocator.dart';

enum LocationStatus {
  INITIALIZING,
  PERMANENTLY_DISABLED,
  DISABLED,
  TURNED_OFF,
  WORKING
}

extension TrackMapTypeMapper on LocationStatus {
  String? toPrettyString() {
    switch (this) {
      case LocationStatus.PERMANENTLY_DISABLED:
        return "Permanently denied";
      case LocationStatus.TURNED_OFF:
        return "Location services disabled";
    }
    return null;
  }
}

class LocationProvider extends ChangeNotifier {
  LocationStatus _locationStatus = LocationStatus.INITIALIZING;

  LocationStatus get locationStatus => _locationStatus;

  bool get locationEnabled => _locationStatus == LocationStatus.WORKING;

  final Stream<Position> locationStream;

  LocationProvider({required this.locationStream}) {
    Geolocator.isLocationServiceEnabled().then((x) {
      if (x) {
        _locationStatus = LocationStatus.WORKING;
        notifyListeners();
      } else {
        this.enableLocation();
      }
    });
  }

  enableLocation() async {
    if (_locationStatus == LocationStatus.WORKING) return;
    var result = await Geolocator.checkPermission();
    if (result == LocationPermission.always ||
        result == LocationPermission.whileInUse) {
      var enabled = await Geolocator.isLocationServiceEnabled();
      if (enabled) {
        _locationStatus = LocationStatus.WORKING;
      } else {
        _locationStatus = LocationStatus.TURNED_OFF;
      }
    } else if (result == LocationPermission.deniedForever) {
      _locationStatus = LocationStatus.PERMANENTLY_DISABLED;
    }
    notifyListeners();
  }
}
