import 'package:flutter/cupertino.dart';
import 'package:flutter_activity_recognition/flutter_activity_recognition.dart';
import 'package:pedometer/pedometer.dart';

class StepsProvider extends ChangeNotifier{
  bool _stepsPermanentlyDisabled = false;
  bool _stepsEnabled = false;

  bool get stepsEnabled => _stepsEnabled;
  bool get stepsPermanentlyDisabled => _stepsPermanentlyDisabled;
  final Stream<StepCount> stepsStream;

  StepsProvider({required this.stepsStream}){
    enableSteps();
  }

  enableSteps()async{
    if(_stepsEnabled) return;
    final activityRecognition = FlutterActivityRecognition.instance;
    activityRecognition.checkPermission().then((x){
      _stepsEnabled = x == PermissionRequestResult.GRANTED;
      _stepsPermanentlyDisabled = x == PermissionRequestResult.PERMANENTLY_DENIED;
      notifyListeners();
    });
  }
}