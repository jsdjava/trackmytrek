import 'package:flutter/cupertino.dart';
import 'package:collection/collection.dart';
import 'package:track_my_trek/providers/settings.dart';

class SettingsModel extends ChangeNotifier {
  late SettingsProvider _settings;

  int _height = 69;
  Sex _sex = Sex.MALE;
  Distance _distance = Distance.CUMULATIVE;
  TrackMapType _trackMapType = TrackMapType.NORMAL;

  int get height => _height;

  Sex get sex => _sex;

  Distance get distance => _distance;

  TrackMapType get trackMapType => _trackMapType;

  set height(value) {
    _height = value ?? _height;
    _settings.saveInt("height", _height);
    notifyListeners();
  }

  set distance(value) {
    _distance = value ?? _distance;
    _settings.saveString("distance", _distance.toString());
    notifyListeners();
  }

  set sex(value) {
    _sex = value ?? _sex;
    _settings.saveString("sex", _sex.toString());
    notifyListeners();
  }

  set trackMapType(value) {
    _trackMapType = value ?? _trackMapType;
    _settings.saveString("trackMapType", _trackMapType.toString());
    notifyListeners();
  }

  SettingsModel(SettingsProvider settings) {
    _settings = settings;
    Future.wait(<Future>[
      settings.get("height"),
      settings.get("sex"),
      settings.get("distance"),
      settings.get("trackMapType"),
    ]).then((values) {
      height = values[0];
      sex = Sex.values.firstWhereOrNull((x) => x.toString() == values[1]);
      distance =
          Distance.values.firstWhereOrNull((x) => x.toString() == values[2]);
      trackMapType = TrackMapType.values
          .firstWhereOrNull((x) => x.toString() == values[3]);
    });
  }
}
