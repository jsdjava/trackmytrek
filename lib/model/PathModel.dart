import 'dart:async';
import 'dart:collection';
import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:track_my_trek/data/TrackPath.dart';
import 'package:track_my_trek/providers/database.dart';
import 'package:track_my_trek/providers/location.dart';
import 'package:track_my_trek/providers/steps.dart';

class PathModel extends ChangeNotifier {
  List<LatLng> _points = [];
  int _steps = 0;
  bool _tracking = false;
  int _stepsOffset = 0;
  int _trackPathId = 0;
  DateTime? _startTime;
  int _elapsedTime = 0;

  Position? curPos;
  StreamSubscription? locationSubscription;
  StreamSubscription? stepsSubscription;
  StreamSubscription? timeSubscription;

  bool get tracking => _tracking;

  UnmodifiableListView<LatLng> get points => UnmodifiableListView(_points);

  int get steps => _steps;

  int get elapsedTime => _elapsedTime;

  late DBProvider _db;
  LatLng? lastStoredPoint;

  PathModel(Stream timeStream, DBProvider db, LocationProvider locationProvider,
      StepsProvider stepsProvider, PathModel? oldPathModel) {
    _db = db;
    if (oldPathModel != null) {
      _points = oldPathModel._points;
      _steps = oldPathModel._steps;
      _tracking = oldPathModel._tracking;
      _stepsOffset = oldPathModel._stepsOffset;
      _trackPathId = oldPathModel._trackPathId;
      _elapsedTime = oldPathModel._elapsedTime;
      _startTime = oldPathModel._startTime;

      curPos = oldPathModel.curPos;
      lastStoredPoint = oldPathModel.lastStoredPoint;

      oldPathModel.locationSubscription?.cancel();
      oldPathModel.stepsSubscription?.cancel();
      oldPathModel.timeSubscription?.cancel();
    }
    if (locationProvider.locationEnabled && stepsProvider.stepsEnabled) {
      stepsSubscription = stepsProvider.stepsStream.listen((event) {
        if (!_tracking) return;
        if (_stepsOffset == 0) {
          _stepsOffset = event.steps;
          return;
        }
        setSteps(event.steps - _stepsOffset);
        db.updatePathSteps(_trackPathId, _steps);
      });
      locationSubscription =
          locationProvider.locationStream.listen((Position position) {
        onPositionChange(position);
      });
      timeSubscription = timeStream.listen((_) {
        if (!_tracking) return;
        _elapsedTime = ((DateTime.now().millisecondsSinceEpoch -
                    _startTime!.millisecondsSinceEpoch) /
                1000)
            .floor();
        notifyListeners();
        db.updatePathTime(_trackPathId, _elapsedTime);
      });
    }
  }

  getPath() {
    return new TrackPath(
        id: _trackPathId, points: points, steps: steps, time: elapsedTime);
  }

  bool farEnough(LatLng prevPoint, LatLng curPoint) {
    double distSquared = (pow(prevPoint.longitude - curPoint.longitude, 2) +
            pow(prevPoint.latitude - curPoint.latitude, 2))
        .toDouble();
    return distSquared * 1000000000000 > 10000;
  }

  onPositionChange(Position position) {
    curPos = position;
    if (!_tracking) return;
    // points should never be empty
    var curPoint = new LatLng(position.latitude, position.longitude);
    var middlePoint = _points.last;
    if (lastStoredPoint == middlePoint) {
      // we're right at the edge, add on dog
      addPoint(curPoint);
    } else {
      setLastPoint(curPoint);
    }
    var firstPoint = points[points.length - 2];
    if (farEnough(firstPoint, curPoint)) {
      lastStoredPoint = curPoint;
      _db.addPointToPath(curPoint, _trackPathId);
      // if colinear prevPoint, firstPoint, curPoint, then update instead of add
    }
  }

  stopTracking() {
    _tracking = false;
    notifyListeners();
  }

  startTracking() async {
    // reset results from previous path
    _steps = 0;
    _stepsOffset = 0;
    _elapsedTime = 0;

    // setup vals for new path
    var startPos =
        curPos ?? await GeolocatorPlatform.instance.getCurrentPosition();
    var curPoint = new LatLng(startPos.latitude, startPos.longitude);
    _startTime = DateTime.now();
    lastStoredPoint = curPoint;
    _trackPathId = await _db.createPath();
    _points = [..._points, curPoint];
    _db.addPointToPath(curPoint, _trackPathId);
    _tracking = true;
    notifyListeners();
  }

  void setLastPoint(LatLng point) {
    if (_tracking) {
      _points = [..._points.sublist(0, _points.length - 1), point];
      notifyListeners();
    }
  }

  void addPoint(LatLng point) {
    if (_tracking) {
      _points = [..._points, point];
      notifyListeners();
    }
  }

  void setSteps(int steps) {
    if (_tracking) {
      _steps = steps;
      notifyListeners();
    }
  }
}
