import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:track_my_trek/providers/database.dart';
import 'package:track_my_trek/providers/location.dart';

enum LockModelState {
  NO_POSITION,
  DB_POSITION,
  STREAM_POSITION,
}

class LockModel extends ChangeNotifier {
  bool _locked = true;

  bool get locked => _locked;

  set locked(x) {
    if(_locked == x) return;
    _locked = x;
    lockCamera();
    notifyListeners();
  }

  GoogleMapController? _mapController;
  LatLng? _lockPos;
  LockModelState _lockModelState = LockModelState.NO_POSITION;
  StreamSubscription? locationSubscription;

  LockModel(LocationProvider locationProvider, DBProvider dbProvider, LockModel? oldModel){
    if(oldModel !=null) {
      _locked = oldModel._locked;
      _mapController = oldModel._mapController;
      _lockPos = oldModel._lockPos;
      _lockModelState = oldModel._lockModelState;
      oldModel.locationSubscription?.cancel();
    }
    if(locationProvider.locationEnabled){
        if(_lockModelState == LockModelState.NO_POSITION){
          dbProvider.getFirstDbPosition().then((x){
            if(_lockModelState == LockModelState.NO_POSITION) {
              if (x != null) {
                _lockPos = x;
                lockCamera(withZoom: true);
              }
              _lockModelState = LockModelState.DB_POSITION;
            }
          });
        }
        locationSubscription = locationProvider.locationStream.listen((event) {
          _lockPos = new LatLng(event.latitude, event.longitude);
          lockCamera(withZoom: _lockModelState!=LockModelState.STREAM_POSITION);
          _lockModelState = LockModelState.STREAM_POSITION;
        });
    }
  }

  lockCamera({bool withZoom=false}) {
    if (_mapController != null && _lockPos != null && _locked) {
      _mapController!.animateCamera(withZoom ? CameraUpdate.newLatLngZoom(_lockPos!, 16) : CameraUpdate.newLatLng(_lockPos!));
    }
  }

  registerMapController(mapController) {
    _mapController = mapController;
    lockCamera(withZoom:true);
  }
}
