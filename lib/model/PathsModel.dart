import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import 'package:track_my_trek/data/TrackPath.dart';
import 'package:track_my_trek/providers/database.dart';
import 'package:track_my_trek/algorithms/DouglasPeucker.dart';

class PathsModel extends ChangeNotifier {
  PathsModel(DBProvider db) {
    db.getDbPathsStream().listen((paths) {
      addPaths(paths);
    });
    db.getDbSteps().then((steps) => setSteps(steps));
    db.getDbTotalPaths().then((totalPaths) => setTotalPaths(totalPaths));
    db.getDbTime().then((time) => setTime(time));
  }

  // Could be unmodifiable..., a little worried about perf
  List<TrackPath> previousPaths = [];
  int _previousSteps = 0;
  int _totalPaths = 0;
  int _time = 0;

  int get totalPaths => _totalPaths;

  int get previousSteps => _previousSteps;

  int get time => _time;

  void setTotalPaths(int totalPaths) {
    this._totalPaths = totalPaths;
    notifyListeners();
  }

  void setSteps(int steps) {
    this._previousSteps = steps;
    notifyListeners();
  }

  void setTime(int time) {
    this._time = time;
    notifyListeners();
  }

  void addPaths(List<TrackPath> paths) {
    previousPaths = [
      ...previousPaths,
      // ...paths,
      ...paths.map((p) => smoothPath(p)),
      // ...List.generate(1000,(p)=>smoothPath(fuzzPath(paths[Random().nextInt(paths.length-1)])))
    ];
    notifyListeners();
  }

  /*bool fitsLine(List<LatLng> points) {
    var lats = points.map((x) => x.latitude).toList();
    var lons = points.map((x) => x.longitude).toList();
    var solver = new LeastSquaresSolver(
        lats, lons, List.generate(lats.length, (index) => 1));
    var result = solver.solve(1);
    return (result?.confidence ?? 1) >= 0.85;
  }*/

  /*TrackPath fuzzPath(TrackPath path){
    var fuzzedPoints = path.points.map((x)=>new LatLng(x.latitude+(1-Random().nextDouble())/50, x.longitude+(1-Random().nextDouble())/50));
    var fuzzedPath = new TrackPath(id: path.id*100, steps: path.steps, points: fuzzedPoints.toList());
    return fuzzedPath;
  }*/

  TrackPath smoothPath(TrackPath path) {
    var pathPoints =
        path.points.map((p) => new Point(p.latitude, p.longitude)).toList();
    //0.001 is pretty good (764 pts -> 50 pts total for about 1.25 miles)
    List<Point> douglasPoints = DouglasPeucker.simplify(pathPoints,
        tolerance: 0.00015, highestQuality: false);
    return new TrackPath(
        id: path.id,
        steps: path.steps,
        time: path.time,
        points: douglasPoints.map((d) => new LatLng(d.x, d.y)).toList(),
        color: Colors.blue);
    /*List<LatLng> smoothedPoints = [];
    List<LatLng> curPoints = [];
    for (var i = 0; i < path.points.length; i++) {
      if (curPoints.length < 2) {
        curPoints.add(path.points[i]);
      } else {
        if (farEnough(curPoints.first, path.points[i])) {
          if (fitsLine([...curPoints, path.points[i]])) {
            curPoints.add(path.points[i]);
          } else {
            smoothedPoints.addAll([curPoints.first, curPoints.last]);
            curPoints = [path.points[i]];
          }
        } else{
          curPoints.add(path.points[i]);
        }
      }
    }
    if (curPoints.isNotEmpty) {
      smoothedPoints.add(curPoints.first);
    }
    if (curPoints.length > 1) {
      smoothedPoints.add(curPoints.last);
    }
    return new TrackPath(
        id: path.id * 1000,
        steps: path.steps,
        points: smoothedPoints,
        color: Colors.green,
        zIndex: 20);*/
  }

  void addPath(TrackPath path) {
    previousPaths = [...previousPaths, smoothPath(path)];
    _totalPaths += 1;
    _previousSteps += path.steps;
    _time += path.time;
    notifyListeners();
  }
}
